<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessageSent;

class ChatController extends Controller
{
    private $user_name;

    public function enter_chat(Request $request){
        $this->user_name = $request->name;
        return $this->index();
    }
    private function index(){
        return view('chat', ['user' => $this->user_name]);
    }
    public function send_msg(Request $request){
        broadcast(new MessageSent(['user' => 'Abuba', 'text' => 'gag']))->toOthers();
        return true;
    }
}
