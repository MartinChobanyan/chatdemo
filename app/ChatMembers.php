<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMembers extends Model
{
    public function messages(){
        return $this->hasMany(Messages::class, 'member_id', 'id');
    }
}
