require('./bootstrap');

window.Vue = require('vue');

Vue.component('enter-chat', require('./components/EnterChat.vue').default);
Vue.component('chat', require('./components/Chat.vue').default);

const app = new Vue({
    el: '#app'
});
